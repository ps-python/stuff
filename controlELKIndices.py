#!/usr/bin/env python3
# -*- coding: utf-8 -*-


#  +-- bm-monitor ----------------------------------------------------------------+
#  |                                                                              |
#  |  _                                                                           |
#  | | |                                                _                         | 
#  | | |___  _ __ ___          _ __ ___   ___  _ __  _ | |    ___  _ _            |
#  | |  _  \| '_ ` _ \   __   | '_ ` _ \ / _ \| '_ \| ||  _| / _ \| `_|           |
#  | | |_| || | | | | | |__|  | | | | | | |_| | | | | || |__ ||_||| |             |
#  | |_____/|_| |_| |_|       |_| |_| |_|\___/|_| |_|_||____|\___/|_|             |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * bm-monitor is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  bm-monitor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


"""
NAGIOS Script : check Elasticseach indexes health  
    
Installation:
    This script needs additional lib :
        requests (Cmd install : pip install requests)
        
        
After :
    4 status cases  returned :
        0 : OK : This Cluster indexes are safe (green state)
        1 : WARNING : Something wrong with this cluser indexes (yellow state)
        2 : CRITICAL : Unavailable to connect to the node
        2 : CRITICAL : Connection to the node is OK, but one or more indexes are in "red" state or "read_only_allow_delete" state

Usage :
    python3 controlELKIndices.py [ -e|--elasticServer "bm4-deb9.intra" ] [ -t|--httpProto http|https ] [ -p|--elasticPort 9200 ] [ -u|--user toto ] [ -w|--password somePassword ] [ -v|--verifyPeer True|False ]
    

"""


# Python lib
from collections import defaultdict
import getopt
import json
import os
import requests 
import sys





"""
PARAMS : init the default params in this script 

"""
params = defaultdict(dict)
params = {'elasticServer': "127.0.0.1",
          'httpProto': "http",
          'elasticPort': "9200",
          'verifyPeer': False, #Check or not the server certificate
          }






"""
getopt
"""
options, remainder = getopt.getopt(sys.argv[1:], 'e:t:p:u:w:v:', 
                                   ['elasticServer=', 
                                    'httpProto=',
                                    'elasticPort=',
                                    'user=',
                                    'password=',
                                    'verifyPeer=',
                                    ])




for opt, arg in options:
    if opt in ('-e', '--elasticServer'):
        params['elasticServer'] = arg
        
    elif opt in ('-t', '--httpProto'):
        params['httpProto'] = arg
        
    elif opt in ('-p', '--elasticPort'):
        params['elasticPort'] = arg
        
    elif opt in ('-u', '--user'):
        params['user'] = arg
    
    elif opt in ('-w', '--password'):
        params['password'] = arg
        
    elif opt in ('-v', '--verifyPeer'):
        params['verifyPeer'] = arg



"""
CLASS

"""

class controELKServices():
    def __init__(self):
        self.params = defaultdict(dict)



    def setParams(self,**kwargs):
        if 'elasticServer' in kwargs.keys():
            self.params['elasticServer'] = kwargs['elasticServer']

        if 'httpProto' in kwargs.keys():
            self.params['httpProto'] = kwargs['httpProto']

        if 'elasticPort' in kwargs.keys():
            self.params['elasticPort'] = kwargs['elasticPort']

        if 'user' in kwargs.keys():
            self.params['user'] = kwargs['user']

        if 'password' in kwargs.keys():
            self.params['password'] = kwargs['password']

        if 'verifyPeer' in kwargs.keys():
            self.params['verifyPeer'] = kwargs['verifyPeer']



    def session(self):
        self.s = requests.Session()
        if self.params['user'] and self.params['password']:        
            self.s.auth = ('user', 'pass')



    def connectToELK(self):
        STATUS = [ None, None]
        URL = self.params['httpProto'] + "://" + self.params['elasticServer']
        
        if len(self.params['elasticPort']) > 0:
            URL += ":" + self.params['elasticPort']
        
        URL += "/" 
        
        try:
            test = self.s.get(URL, verify=self.params['verifyPeer'], timeout=5).json()
            if 'cluster_name' in test.keys():
                STATUS = ['OK', "connected to cluster " + test['cluster_name']]
        except requests.exceptions.HTTPError as errh:
            STATUS = [None, errh]
        except requests.exceptions.ConnectionError as errc:
            STATUS = [None, errc]
        except requests.exceptions.Timeout as errt:
            STATUS = [None, errt]
        except requests.exceptions.RequestException as err:
            STATUS = [None, err]
            
        return STATUS
    
    
    
    def retrieveIndexStatus(self):
        STATUS = [ None, None]
        URL = self.params['httpProto'] + "://" + self.params['elasticServer']
        
        if len(self.params['elasticPort']) > 0:
            URL += ":" + self.params['elasticPort']
        
        URL += "/_cat/indices?format=json" 
        
        try:
            test = self.s.get(URL, verify=self.params['verifyPeer'], timeout=5).json()
            STATUS = self.retrieveMainStatus(test)
                
        except requests.exceptions.HTTPError as errh:
            STATUS = [None, errh]
        except requests.exceptions.ConnectionError as errc:
            STATUS = [None, errc]
        except requests.exceptions.Timeout as errt:
            STATUS = [None, errt]
        except requests.exceptions.RequestException as err:
            STATUS = [None, err]
            
        return STATUS

    
    
    def retrieveIndexSettings(self, indexName):
        STATUS = None
        URL = self.params['httpProto'] + "://" + self.params['elasticServer']
        
        if len(self.params['elasticPort']) > 0:
            URL += ":" + self.params['elasticPort']
        
        URL += "/" + indexName + "/_settings" 
        
        try:
            test = self.s.get(URL, verify=self.params['verifyPeer'], timeout=5).json()
           
            if 'blocks' in test[indexName]['settings']['index'].keys():
                if 'read_only_allow_delete' in test[indexName]['settings']['index']['blocks'].keys():
                    STATUS = test[indexName]['settings']['index']['blocks']['read_only_allow_delete']
        except:
            pass
        
        return STATUS
    
    
    
    def retrieveMainStatus(self,test):
        STATUS = [ "green", "All indexes are fine"]
        colorToNum = { "green": 0, "yellow": 1, "red":2 }
        numToColor = ["green", "yellow", "red"]
        
        prevNumColor = 0
        numColor = 0
        for idx in test:
            numColor = colorToNum[idx['health']]

            if numColor > prevNumColor:
                STATUS[0] = idx['health']
                STATUS[1] = "Error on index named " + idx['index']
                prevNumColor = numColor
            
            if self.retrieveIndexSettings(idx['index']) is not None and "true" in self.retrieveIndexSettings(idx['index']):
                STATUS[0] = 'red'
                STATUS[1] = "Error, index named \"" + idx['index'] + "\" is read_only_allow_delete state"
                prevNumColor = 2
    
        return STATUS



"""
Call the test
"""

t = controELKServices()
t.setParams(**params)
t.session()



## Step 1 : try to connect ELK Server
tcs = t.connectToELK()

if tcs[0] is None:
    print("CRITICAL : " + str(tcs[1]))
    exit(2)



### Step 2 : get cluster indexes status 
hcs = t.retrieveIndexStatus()

# Case of error while connection
if hcs[0] is None:
    print("CRITICAL : " + str(hcs[1]))
    exit(2)


# Case of safe cluster
if hcs[0] == 'green':
    print("OK : " + str(hcs[1]))
    exit(0)


# Case of problem on cluster
if hcs[0] == 'yellow':
    print("WARNING : " + str(hcs[1]))
    exit(1)
    
    
# Case of cluster out-of-order
if hcs[0] == 'red':
    print("CRITICAL : " + str(hcs[1]))
    exit(2)


