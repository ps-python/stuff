#!/usr/bin/env python3
# -*- coding: utf-8 -*-


#  +-- bm-monitor ----------------------------------------------------------------+
#  |                                                                              |
#  |  _                                                                           |
#  | | |                                                _                         | 
#  | | |___  _ __ ___          _ __ ___   ___  _ __  _ | |    ___  _ _            |
#  | |  _  \| '_ ` _ \   __   | '_ ` _ \ / _ \| '_ \| ||  _| / _ \| `_|           |
#  | | |_| || | | | | | |__|  | | | | | | |_| | | | | || |__ ||_||| |             |
#  | |_____/|_| |_| |_|       |_| |_| |_|\___/|_| |_|_||____|\___/|_|             |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * bm-monitor is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  bm-monitor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


"""
NAGIOS Script : check Exchange ActiveSync (aka EAS) service   
    
Installation:
    This script needs additional lib :
        requests (Cmd install : pip install requests)
    
    
After :
    Status cases  returned :
        0 : OK       : The connection and the autentication are OK
        2 : CRITICAL : Unavailable to connect to the server
        2 : CRITICAL : Connected to server, but something else wrong
        3 : UNKNOWN  : Something wrong with account params (account &/or password) (errno 401)
        3 : UNKNOWN  : The partnership is not set yet (errno 403)

Usage :
    python3 controlEAS.py [ -e|--easServer ] [ -t|--httpProto ] [ -d|--domain ] [ -u|--user ] [ -w|--password ] [ -v|--verifyPeer ] [ -i|--deviceId ] [ -T|--deviceType ]

    By default :
        -t|--httpProto  set to https
        -v|--verifyPeer set to False (does not verify peer server certificate)
        -T|--deviceType set to Android
        
    Example : 
        python3 controlEAS.py --easServer="bm35-deb9.intra" --domain='bm35-deb9.intra' --user='checkeas' --password='password1234' --deviceId='android123456780'
    

Caution :
    Do not use special caracters as %! or $ for account password.
    While Bluemind provides API key for account, create one.  
"""


# Python lib
import base64
from collections import defaultdict
import getopt
import json
import os
import requests 

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import sys
import urllib




"""
PARAMS : init the default params in this script 

"""
params = defaultdict(dict)
params = {'httpProto': "https",
          'deviceType':'Android',
          'verifyPeer': False, #Check or not the server certificate
          }



"""
getopt
"""
options, remainder = getopt.getopt(sys.argv[1:], 'e:t:d:u:w:v:i:T:', 
                                   ['easServer=', 
                                    'httpProto=',
                                    'domain=',
                                    'user=',
                                    'password=',
                                    'verifyPeer=',
                                    'deviceId=',
                                    'deviceType=',
                                    ])




for opt, arg in options:
    if opt in ('-e', '--easServer'):
        params['easServer'] = arg
        
    elif opt in ('-t', '--httpProto'):
        params['httpProto'] = arg
        
    elif opt in ('-d', '--domain'):
        params['domain'] = arg
        
    elif opt in ('-u', '--user'):
        params['user'] = arg
    
    elif opt in ('-w', '--password'):
        params['password'] = urllib.parse.quote(arg)
        
    elif opt in ('-v', '--verifyPeer'):
        params['verifyPeer'] = arg

    elif opt in ('-i', '--deviceId'):
        params['deviceId'] = arg
        
    elif opt in ('-T', '--deviceType'):
        params['deviceType'] = arg

"""
CLASS

"""

class checkEAS():
    def __init__(self):
        self.params = {}

    def setParams(self,**kwargs):
        if 'easServer' in kwargs.keys():
            self.params['easServer'] = kwargs['easServer']

        if 'httpProto' in kwargs.keys():
            self.params['httpProto'] = kwargs['httpProto']

        if 'domain' in kwargs.keys():
            self.params['domain'] = kwargs['domain']

        if 'user' in kwargs.keys():
            self.params['user'] = urllib.parse.quote(kwargs['user'])

        if 'password' in kwargs.keys():
            self.params['password'] = urllib.parse.quote(kwargs['password'])

        if 'verifyPeer' in kwargs.keys():
            self.params['verifyPeer'] = kwargs['verifyPeer']

        if 'deviceId' in kwargs.keys():
            self.params['deviceId'] = kwargs['deviceId']

        if 'deviceType' in kwargs.keys():
            self.params['deviceType'] = kwargs['deviceType']


    def setHeader(self):
        self.header  = {'Content-Type': "application/vnd.ms-sync.wbxml", 'MS-ASProtocolVersion': '14', 'User-Agent': 'checkEAS-plugin'} 
        self.header['Expec']      = "100-continue" 
        self.header['Host']       = self.params['easServer']
        self.header['Connection'] = "Keep-Alive"
        
        Authorization = base64.urlsafe_b64encode(bytes(self.params['domain'] + '\\' + self.params['user'] + '@' + self.params['domain'] + ':' + self.params['password'], "utf-8")).decode()
        self.header['Authorization'] = "Basic " + Authorization


    def session(self):
        self.s = requests.Session()
    

    def controlServerAccess(self):
        STATUS = [ None, None]
        URL  = self.params['httpProto'] + '://' + self.params['easServer'] + '/Microsoft-Server-ActiveSync'
        
        try:
            test = self.s.post(URL, verify=self.params['verifyPeer'], timeout=5)
            STATUS = ['OK', "connected to server " + self.params['easServer']]
        except requests.exceptions.HTTPError as errh:
            STATUS = [None, errh]
        except requests.exceptions.ConnectionError as errc:
            STATUS = [None, errc]
        except requests.exceptions.Timeout as errt:
            STATUS = [None, errt]
        except requests.exceptions.RequestException as err:
            STATUS = [None, err]
            
        return STATUS
    
    
    
    def connectToEAS(self):
        STATUS = [ None, None]
        URL  = self.params['httpProto'] + '://' + self.params['easServer'] + '/Microsoft-Server-ActiveSync'
        URL += '?User=' + self.params['user'] + urllib.parse.quote('@') + self.params['domain']
        URL += '&DeviceId=' + self.params['deviceId']
        URL += '&DeviceType=' + self.params['deviceType']
        URL += '&Cmd=Sync'
        
        try:
            test = self.s.post(URL, verify=self.params['verifyPeer'], headers=self.header,timeout=5)
    
            if test.status_code == 200:
                STATUS = ['OK', 'Logged to Exchange ActiveSync Server']
                
            if test.status_code == 401:
                STATUS = ['NOK', 'Authentication failed, verify account &/or password' ]
            
            if test.status_code == 403:
                STATUS = ['NOK', 'Access failed, verify partnership for ' + self.params['deviceId'] ]
                
            if test.status_code == 502:
                STATUS = [None, 'Acess to server is OK but the service EAS does not respond !!!']

        except requests.exceptions.HTTPError as errh:
            STATUS = [None, errh]
        except requests.exceptions.ConnectionError as errc:
            STATUS = [None, errc]
        except requests.exceptions.Timeout as errt:
            STATUS = [None, errt]
        except requests.exceptions.RequestException as err:
            STATUS = [None, err]
            
        return STATUS




eas = checkEAS()
eas.session()
eas.setParams(**params)


## Step 1 : try to connect EAS Server
tcs = eas.controlServerAccess()
if tcs[0] is None:
    print("CRITICAL : " + str(tcs[1]))
    exit(2)



### Step 2 : try a sync command
eas.setHeader()
tsc = eas.connectToEAS()

if tsc[0] is 'OK':
    print("OK : " + str(tsc[1]))
    exit(0)


if tsc[0] is None:
    print("CRITICAL : " + str(tsc[1]))
    exit(2)


if tsc[0] is 'NOK':
    print("UNKNOWN : " + str(tsc[1]))
    exit(3)
