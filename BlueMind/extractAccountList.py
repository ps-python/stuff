#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
*------------------------------------------------------------------------
* extractAccountList
*------------------------------------------------------------------------
* @license : GNU Affero General Public License Version3 <http://www.gnu.org/licenses>
* @author  : Pascal SALAUN <pascal.salaun@bm-monitor.org>
* @Website : http://bm-monitor.org
*------------------------------------------------------------------------
"""
################################ Define all options here ################################ 

## BLUEMIND

# List of servers (IP or FQDN)
servers               = ['192.168.122.205'] 

# Common domain & admin account to all servers
adminAccount          = {
                            'domain': 'bm4-deb9.intra' , 
                            'admin': 'admin@bm4-deb9.intra' , 
                            'adminPwd' : 'admin'
                        } # Must be same on all servers


# Disable certificate control
VerifyPeer = False 

# Define a specific source name (cf BM core.log)
origin = "bm-monitor-extractAccountList" 

# Define the common headers values to all requests. The session token 'X-BM-ApiKey' will be added after. 
Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache'} 


################################ Define all Python lib import ################################ 

# Native Python Lib/Class
from collections import defaultdict
import csv
from datetime import datetime
import encodings
import getopt 
import hashlib
import json
import os.path 
import sys
import time
import uuid

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

sys.path.insert(0 ,'./jsonQuery')
import jsonQuery


################################ Define all functions here ################################ 



"""
(obj) openWriteFile:
    open a file object in "write" mode, and return it
    Args:
        outputfile : the path and name to res file

"""
def openWriteFile(outputfile):
    f = open(outputfile,'w')
    return f



"""
(dict) getToken :
    call BM servers and ask a session token and return all in a dict {'serverA': 'tokenA', 'serverB': 'tokenB',...}
    Return a dict of token (value) by server (key)
    kwargs:  
        servers    : list servers defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        admin      : the same admin account available on each server
        adminPwd   : its password
"""
def getToken(**kwargs): 
    Verify   = kwargs['VerifyPeer']
    response = {}
    response = {'authKey': None}
    
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': "application/json"}
    postUri = '/auth/login?login=' + kwargs['admin'] + '&origin=' + kwargs['origin']

    Data = None
    Data = json.dumps(kwargs['adminPwd'])

    for server in kwargs['servers']:
        base = 'https://' + server + '/api'
        
        r = requests.post( base + postUri,verify=Verify, headers=Headers, data = Data ).json()

        if r['authKey']:
            response[server] = {}
            response[server]['authKey'] = r['authKey']
            if 'authKey' in response.keys():
                del response['authKey']
      
    return response 



"""
(dict) getAllUid :
    Retrieve all logins (aka account) and uids from server
    then retrieve for each one their aBooks
    Return a dict { 'name': { uid : [{aBook_name : aBook_uid},]}, }
    kwargs :
        server
        userAuthKey
        VerifyPeer
        domainUid
    
"""
def getAllUid(**kwargs):
    base="https://" + kwargs['server'] + "/api"
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']

    results = defaultdict(dict) 

    postUri = '/users/' + kwargs['domainUid'] + '/_alluids'

    r = requests.get( base + postUri,verify=Verify, headers=Headers).json()
    
    for entry in r:
        if entry != "bmhiddensysadmin":
            values = getAccountNameFromUID(uid=entry, **dict(kwargs))
            
          
            if (values['login'] is not None) and (values['login'] != "admin"):
                login = values['login']
                ou    = values['ou']
                results[login] = ou
    
    return results
          


"""
(str) getAccountNameFromUID:
    Retrieve and return the login from its UID 
    kwargs :
        server
        userAuthKey
        VerifyPeer
        domainUid
        uid
"""
def getAccountNameFromUID(**kwargs):
    value= {}
    base="https://" + kwargs['server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = VerifyPeer

    results = {} 

    getUri = '/users/' + kwargs['domainUid'] + "/" +  kwargs['uid'] + "/complete"
    
    r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    
    if r['value']['login'] is not None:
        value['login'] = r['value']['login']
        if r['value']['orgUnitUid'] is not None :
            value['ou']    = r['value']['orgUnitUid']
        else:
            value['ou'] = "root"
    
    return value
 


def getAllLogin(**kwargs):
    for server in  kwargs['serverListToken'].keys():
        params             = kwargs
        params['server']   = server
        params['authKey']  = kwargs['serverListToken'][server]['authKey']

        allUsers = {}
        allUsers  = getAllUid(**kwargs)
        
        
        for login in allUsers.keys():
            trace = []
            trace.append(login)

            Line = ",".join(trace)
            Line += "\n"
            kwargs['outputFileDescriptor'].write(Line)



"""
(void) main:
    the main function, in which all stuff is done/called

"""
def main(argv, **kwargs):
    inputfile = None
    outputfile = None


    # Define here "blabla" when options are not defined or help called 
    usage='''
        Use this script as :

            python extractAccountList.py -o|--ofile <outputfile> [-h] [-v]
    
        Where:
             <outputfile> : is the name of the file where to store results
    '''

    try:
        opts, args = getopt.getopt(argv,"ho:v",["ofile="])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    Verbose = False 

    for opt, arg in opts:
        if opt == '-h':
            print(usage) 
            sys.exit()
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt == '-v':
            Verbose = True

    if  outputfile is None:
        print(usage) 
        sys.exit()

    # Set all common params to all servers 
    params = kwargs 
    params['Verbose'] = Verbose
    params['admin']    = kwargs['adminAccount']['admin']
    params['adminPwd'] = kwargs['adminAccount']['adminPwd']
    params['serverListToken'] = getToken(**dict(params))

    params['domainUid'] = kwargs['adminAccount']['domain']


    #Open result file
    f = openWriteFile(outputfile)
    params['outputFileDescriptor'] = f

    getAllLogin(**dict(params))

    f.close

################################ The Main Program start here ################################ 
 


if __name__ == "__main__":    
    
    params = { 'servers' : servers ,
               'adminAccount' : adminAccount,
               'VerifyPeer':VerifyPeer,
               'origin':origin,
               'Headers': Headers,
               'js': jsonQuery.js, 
               }
    
    main(sys.argv[1:], **dict(params))
