#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
*------------------------------------------------------------------------
* syncBMAccounts_MBox
*------------------------------------------------------------------------
* @license : GNU Affero General Public License Version3 <http://www.gnu.org/licenses>
* @author  : Pascal SALAUN <pascal.salaun@bm-monitor.org>
* @Website : http://bm-monitor.org
*------------------------------------------------------------------------
"""

# Version : 1.0.0

################################ Define all Python lib import ################################ 

# Native Python Lib/Class
#from concurrent.futures import ThreadPoolExecutor, wait, as_completed
import csv
import getopt 
import json
import sys


import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


################################ Define all options here ################################ 


## BLUEMIND

# Server (IP or FQDN) 
server                = 'bm4-deb9.intra'

# Common domain & admin account 
adminAccount          = {
                            'domain': 'bm4-deb9.intra' , 
                            'admin': 'admin@bm4-deb9.intra' , 
                            'adminPwd' : 'admin'
                        } 

# Disable certificate control
VerifyPeer = False 


# Define the common headers values to all requests. The session token 'X-BM-ApiKey' will be added after. 
Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache'} 


# Just for BM core logs 
origin = "setMoxAcl"

################################ Define all functions here ################################ 


"""
(str) readCsv:
    read content of <inputfile> and return line per line expunged of "\n"
    args: 
        inputfile : the full path to the CSV file
"""
def readCsv(inputfile):
    with open(inputfile, mode='r') as infile:
        spamreader = csv.reader(infile, delimiter=';')
        for row in spamreader:
            yield row



"""
(dict) getToken :
    call BM server  and ask a session token and return a dict {'authKey': <string>, 'error': <string if error>}
    Return a dict of token (value) by server (key)
    kwargs:  
        server     : the server defined in global options
        origin     : a name for core.log (defined in options)
        VerifyPeer : True or False, disable or not server cert control
        admin      : the same admin account available on each server
        adminPwd   : its password
"""
def getToken(**kwargs):     
    Verify   = kwargs['VerifyPeer']
    server   = kwargs['server']
    response = {}
    response = {'error': None}
   
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': "application/json"}
    postUri = '/auth/login?login=' + kwargs['admin'] + '&origin=' + kwargs['origin']

    Data = None
    Data = json.dumps(kwargs['adminPwd'])

    base = 'https://' + server + '/api'
        
    try :
        r = requests.post( base + postUri,verify=Verify, headers=Headers, data = Data ).json()

        if r['authKey']:
            response['authKey'] = r['authKey']
        else:
            response['error'] = "ERROR : can't retrieve Admin token from " + server
                
    except Exception as error:
        response['error'] = "ERROR : can't retrieve Admin token from " + server
        pass 

    return response 



"""
(str) getUserBMUidFromserver :
    Return the mbox uid
    kwargs: 
        authKey    : the admin session authKey
        server     : list servers defined in global options
        VerifyPeer : True or False, disable or not server cert control
        login      : the login aka mailbox (left part of an email address)
        domain     : its domain (right part of an email address)
"""
def getUserBMUidFromserver(**kwargs): 
    """

    """
    uid = None
    
    BMURI="https://" + kwargs['server'] + "/api"
    base = BMURI
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']
    
    getUri = '/users/' + kwargs['domain'] + "/byEmail/" + kwargs['login'] + "@" + kwargs['domain']

    try:
        r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
        if r['uid'] is not None:        
            return r['uid']
        else:
            return uid
    except:
        return uid
    
    



"""
(str) postMailboxACL:
    post mailbox ACLS (Sharing) to server 
    Return http request answer
    kwargs :
        server     : the server name
        authKey    : the session admin atuhKey 
        domain     : the main domain name        
        uid        : the mailbox uuid
        ACLs       : a list of dicts containing sharing rights 
"""
def postMailboxACL(**kwargs):   
    base="https://" + kwargs['server'] + "/api"

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = VerifyPeer
    
    acl = json.dumps(kwargs['ACLs'])
    
    postUri = '/mailboxes/' + kwargs['domain'] + '/' + kwargs['uid'] + '/_acls'

    r = requests.post( base + postUri,verify=Verify, data=acl, headers=Headers)
    
    return r




    


"""
(void) main:
    the main function, in which all stuff is done/called

"""
def main(argv, **kwargs):
    inputfile = None  
    
    shareRights = {'read':'Read', 'write':'Write', 'all':'All'}

    # Define here "blabla" when options are not defined or help called 
    usage='''
        Use this script as :

            python3 setMBoxAcl.py -i|--ifile <inputfile> [-h] [-v]
    
        Where:
             <inputfile> : is the name of the csv file containing account to share, 
                           as e.g. "albert.einstein;marie.curie:read;ada.lovelace:write;ambroise.pare:all"

    '''

    try:
        opts, args = getopt.getopt(argv,"hi:v",["ifile="])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    Verbose = False 

    for opt, arg in opts:
        if opt == '-h':
            print(usage) 
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt == '-v':
            Verbose = True

    if inputfile is None:
        print(usage)
        sys.exit()


    # Set all common params to all servers 
    params = kwargs 
    params['Verbose'] = Verbose
    params['admin']    = kwargs['adminAccount']['admin']
    params['adminPwd'] = kwargs['adminAccount']['adminPwd']
    params['authKey'] = getToken(**dict(params))['authKey']
    params['domain'] = kwargs['adminAccount']['domain']


    # Read CSV file
    for row in readCsv(inputfile):  
        
        login = row.pop(0)
        uid =  getUserBMUidFromserver(login=login, **params)

        if (len(row) > 0) and uid is not None: 
            postList = []
            for mbx in row:
                Dict = {'subject':'' , 'verb':''}
                mbox, right = mbx.split(":")
                Dict['subject'] = getUserBMUidFromserver(login=mbox, **params)
                Dict['verb']    = shareRights[right.lower()]
                        
                postList.append(Dict)

            #print(postList)
            res = postMailboxACL(ACLs=postList, uid=uid, **params)
            if Verbose:
                line = 'ACLs for ' + login + ', status : '
                if res.status_code == 200:
                    line += ' OK'
                else:
                    line += ' ERROR\n'
                    line += res.text
                    
                print(line)
        else:
            if Verbose:
                line = 'ACLs for ' + login + ', status : ERROR, verify this account!'
                print(line)



################################ The Main Program start here ################################ 
 


if __name__ == "__main__":    
    
    params = { 'server' : server ,
               'adminAccount' : adminAccount,
               'VerifyPeer':VerifyPeer,
               'origin':origin,
               'Headers': Headers,
               }
    
    main(sys.argv[1:], **dict(params))
