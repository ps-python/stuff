#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
*------------------------------------------------------------------------
* subscribeAllUserToABooksList
*------------------------------------------------------------------------
* @license : GNU Affero General Public License Version3 <http://www.gnu.org/licenses>
* @author  : Pascal SALAUN <pascal.salaun@bm-monitor.org>
* @Website : http://bm-monitor.org
*------------------------------------------------------------------------
"""
################################ Define all options here ################################ 

## BLUEMIND

# List of servers (IP or FQDN) where you want to create user accounts, primary first.
servers               = ['192.168.122.205'] 

# Common domain & admin account to all servers
adminAccount          = {
                            'domain': 'bm4-deb9.intra' , 
                            'admin': 'admin@bm4-deb9.intra' , 
                            'adminPwd' : 'admin'
                        } # Must be same on all servers



# Disable certificate control
VerifyPeer = False 

# Define a specific source name (cf BM core.log)
origin = "bm-monitor-subscribeAllUserToABooksList" 

# Define the common headers values to all requests. The session token 'X-BM-ApiKey' will be added after. 
Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache'} 


################################ Define all Python lib import ################################ 

# Native Python Lib/Class
import csv
from datetime import datetime
import encodings
import getopt 
import hashlib
import json
import os.path 
import sys
import time
import uuid

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

sys.path.insert(0 ,'./jsonQuery')
import jsonQuery


################################ Define all functions here ################################ 

"""
(str) readCsv:
    read content of <inputfile> and return line per line expunged of "\n"
    args: 
        inputfile : the full path to the CSV file
"""
def readCsv(inputfile):
    with open(inputfile, mode='r') as infile:
        spamreader = csv.reader(infile, delimiter=';')
        for row in spamreader:
            yield row



"""
(dict) getToken :
    call BM servers and ask a session token and return all in a dict {'serverA': 'tokenA', 'serverB': 'tokenB',...}
    Return a dict of token (value) by server (key)
    kwargs:  
        servers    : list servers defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        admin      : the same admin account available on each server
        adminPwd   : its password
"""
def getToken(**kwargs): 
    Verify   = kwargs['VerifyPeer']
    response = {}
    response = {'authKey': None}
    
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': "application/json"}
    postUri = '/auth/login?login=' + kwargs['admin'] + '&origin=' + kwargs['origin']

    Data = None
    Data = json.dumps(kwargs['adminPwd'])

    for server in kwargs['servers']:
        base = 'https://' + server + '/api'
        response[server] = {}

        r = requests.post( base + postUri,verify=Verify, headers=Headers, data = Data ).json()

        if r['authKey']:
            response[server]['authKey'] = r['authKey']
            if 'dataLocation' in  r['authUser']['value'].keys():
                response[server]['dataLocation'] = r['authUser']['value']['dataLocation']
            else:
                response[server]['dataLocation'] = None
            
            if 'authKey' in response.keys():
                del response['authKey']
     


    return response 




"""
(list) retrieveUserUuid : 
    retrieve user uuid for a domain and return it
    kwargs :
        server     : the server or domain
        domainUid  : the domain uid (the FQDN)
        authKey    : the session token for the server
        VerifyPeer : True or False, disable or not server cert control

"""
def retrieveUserUuid(**kwargs):    
    uid = None 
    Verify    = kwargs['VerifyPeer']

    base = "https://" + kwargs['server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}

    getUri = '/users/' + kwargs['domainUid'] + '/byLogin/' + kwargs['login'] + '@' + kwargs['domainUid']
    r = requests.get( base + getUri,verify=Verify,headers=Headers).json()

    if r['uid']:
        uid = r['uid']
    
    return uid




"""
(str) updateUser : 
    call user creation sequence on selected server(s)
    kwargs :
        serverListToken     : dict containing servers and its session token
        domainUid           : the domain uid (the FQDN)
        authKey             : the session token for the server
        VerifyPeer          : True or False, disable or not server cert control    
"""
def updateUser(**kwargs): 
    for server in  kwargs['serverListToken'].keys():

        params                 = kwargs
        params['server']       = server
        params['authKey']      = kwargs['serverListToken'][server]['authKey']
        params['uid']          = retrieveUserUuid(**kwargs)

        ##6 Subscribe User to Domain domainAbooks
        if len(kwargs['domainAbooks']) > 0 :
            for abookName in kwargs['domainAbooks']:
                params['domainAbookName'] = abookName
                params['abookUid'] = ""
                params['abookUid'] = retrieveDomainABooksUid(**dict(params))
                if len(params['abookUid']) > 0:
                    subscribeUserToDomainAbook(**dict(params))
                    logString = "Subscribe " + kwargs['login'] + " to Domain ABook \""  + abookName + "\" on server " + params['server'] + "\n"

                    if kwargs['Verbose']:
                        print(logString)



"""
(str) retrieveDomainABooksUid(**kwargs)
    return the akook UID from its name 
    kwargs :
        server
        userAuthKey
        VerifyPeer
        domainAbookName
        domainUid

"""
def retrieveDomainABooksUid(**kwargs):
    base="https://" + kwargs['server'] + "/api"
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']
   
     
    jsonQuery = {
        'name': kwargs['domainAbookName'],
        'type': 'addressbook'
        }
    data = json.dumps(jsonQuery)
   
    postUri = '/containers/_manage/_listLight'

    r = requests.post( base + postUri,verify=Verify, data=data , headers=Headers).json()
    aBookUid = r[0]['uid']
        
    return aBookUid


"""
(void) subscribeUserToDomainAbook(**kwargs)
    return the akook UID from its name 
    kwargs :
        server
        authKey
        VerifyPeer
        domainAbookName
        domainUid
        uid
"""
def subscribeUserToDomainAbook(**kwargs):         
    base="https://" + kwargs['server'] + "/api"

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']

    js = json.dumps([ { 'containerUid': kwargs['abookUid'], 'offlineSync': False }])

    postUri = '/users/' + kwargs['domainUid'] + '/subscriptions/' + kwargs['uid'] + '/_subscribe'
    retPost = requests.post( base + postUri,verify=VerifyPeer, data=js, headers=Headers)



"""
(void) main:
    the main function, in which all stuff is done/called

"""
def main(argv, **kwargs):
    inputfile = None
    outputfile = None


    # Define here "blabla" when options are not defined or help called 
    usage='''
        Use this script as :

            python subscribeAllUserToABooksList.py -i|--ifile <inputfile> [-h] [-v]
    
        Where:
             <inputfile> : is the name of the csv file containing data (user;Abook Name, Other Abook Name)
             Abooks list is comma separated
    '''

    try:
        opts, args = getopt.getopt(argv,"hi:o:v",["ifile=","ofile="])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    Verbose = False 

    for opt, arg in opts:
        if opt == '-h':
            print(usage) 
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt == '-v':
            Verbose = True

    if inputfile is None:
        print(usage)
        sys.exit()


    # Set all common params to all servers 
    params = kwargs 
    params['Verbose'] = Verbose
    params['admin']    = kwargs['adminAccount']['admin']
    params['adminPwd'] = kwargs['adminAccount']['adminPwd']
    params['serverListToken'] = getToken(**dict(params))
    
    params['domainUid'] = kwargs['adminAccount']['domain']


    # Read CSV file
    for row in readCsv(inputfile):  
        
        # Define all params for this account
        params['login']        = row[0]     
        params['domainAbooks'] = row[1].split(",")
        
        # Create the entry on server
        updateUser(**dict(params))
        

################################ The Main Program start here ################################ 
 


if __name__ == "__main__":    
    
    params = { 'servers' : servers ,
               'adminAccount' : adminAccount,
               'VerifyPeer':VerifyPeer,
               'origin':origin,
               'Headers': Headers,
               'js': jsonQuery.js, 
               }
    
    main(sys.argv[1:], **dict(params))
