#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
*------------------------------------------------------------------------
* moveFromOuToOtherOne
*------------------------------------------------------------------------
* @license : GNU Affero General Public License Version3 <http://www.gnu.org/licenses>
* @author  : Pascal SALAUN <pascal.salaun@bm-monitor.org>
* @Website : http://bm-monitor.org
*------------------------------------------------------------------------
"""
################################ Define all options here ################################ 

## BLUEMIND

# Server (IP or FQDN) where you want to update account
server               = 'bm4-deb9.intra'

# Domain & admin account
adminAccount          = {
                            'domain': 'bm4-deb9.intra' , 
                            'admin': 'admin@bm4-deb9.intra', 
                            'adminPwd' : 'admin'
                        } # Must be same on all servers



# Disable certificate control
VerifyPeer = False 

# Define a specific source name (cf BM core.log)
origin = "bm-monitor-massiveUpdateOu" 

# Define the common headers values to all requests. The session token 'X-BM-ApiKey' will be added after. 
Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache'} 


################################ Define all Python lib import ################################ 

# Native Python Lib/Class
from collections import defaultdict
import csv
from datetime import datetime
import encodings
import getopt 
import hashlib
import json
import os.path 
import sys
import time
import uuid

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)



################################ Define all functions here ################################ 


"""
(str) readCsv:
    read content of <inputfile> and return line per line expunged of "\n"
    args: 
        inputfile : the full path to the CSV file
"""
def readCsv(inputfile):
    with open(inputfile, mode='r') as infile:
        spamreader = csv.reader(infile, delimiter=';')
        for row in spamreader:
            yield row


"""
(obj) openWriteFile:
    open a file object in "write" mode, and return it
    Args:
        outputfile : the path and name to res file

"""
def openWriteFile(outputfile):
    f = open(outputfile,'w')
    return f



"""
(str) getToken :
    call BM server and ask a session token and return it
    Return a dict of token (value) by server (key)
    kwargs:  
        server     : the server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        admin      : the admin account available on server
        adminPwd   : its password
"""
def getToken(**kwargs): 
    Verify   = kwargs['VerifyPeer']
    response = None
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': "application/json"}
    postUri = '/auth/login?login=' + kwargs['admin'] + '&origin=' + kwargs['origin']

    Data = None
    Data = json.dumps(kwargs['adminPwd'])

    base = 'https://' + kwargs['server'] + '/api'
        
    r = requests.post( base + postUri,verify=Verify, headers=Headers, data = Data ).json()

    if r['authKey']:
        response = r['authKey']
      
    return response 




"""
(str) getAccountUIDFromLogin:
    Retrieve and return the login from its UID 
    kwargs :
        server
        userAuthKey
        VerifyPeer
        domainUid
        login
"""
def getAccountUIDFromLogin(**kwargs): 
    value= None
    base="https://" + kwargs['server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = VerifyPeer

    results = {} 

    getUri = '/users/' + kwargs['domainUid'] + "/byLogin/" +  kwargs['login']
    
    r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    
    if r['uid'] is not None:
        value = r['uid']

    return value
 



"""
(dict) getAllOU : 
    retrieve all unitOrganization from Domain, and return dict {"ou": "ouUUID", }
    kwargs :
        server     : the server
        domainUid  : the domain uid (the FQDN)
        authKey    : the session token for the server
        VerifyPeer : True or False, disable or not server cert control
"""
def getAllOU(**kwargs):
    Verify     = kwargs['VerifyPeer']

    base = "https://" + kwargs['server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}

    postUri = '/directory/' + kwargs['domainUid'] + '/_search'
    jsquery = {
                'order': {
                    'by': 'kind',
                    'dir': 'asc'
                },
                'kindsFilter': ['ORG_UNIT']
            }


    r = requests.post( base + postUri,verify=Verify, data=json.dumps(jsquery),headers=Headers).json()
    
    orgaUnit = {}
    if 'values' in r.keys():
        for entry in r['values']:
            
            ou  = entry['value']['displayName']
            uid = entry['uid']
            
            if entry['value']['orgUnitPath']:
                ou=entry['value']['orgUnitPath']['name'] + "/" + ou
                sub = entry['value']['orgUnitPath']
                
                if 'parent' in sub:
                    while sub['parent']:
                        ou = sub['parent']['name'] + "/" + ou
                        if  sub['parent'] is not None :
                            sub = sub['parent']

            orgaUnit[ou] = uid

        
    return orgaUnit




"""
(dict) updateUser : 
    update the account, and write 
    kwargs :
        server               : the server
        domainUid            : the domain uid (the FQDN)
        authKey              : the session token for the server
        VerifyPeer           : True or False, disable or not server cert control
        login                : the login/account to update
        ou                   : the ou (human readable) name 
        ouUid                : the uuid of this Ou
        outputFileDescriptor : the descriptor of log file
        Verbose              : True/False if want to display result on screen
"""
def updateUser(**kwargs):
    
    log                = ""
    base = "https://" + kwargs['server']  + "/api"
    Verify     = kwargs['VerifyPeer']

    trace = []
    trace.append(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
        
    userbmUid = getAccountUIDFromLogin(**kwargs)
        
    newValues = {
        'orgUnitUid': kwargs['ouUid']
    }

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
        
    
    ## Step 1 retrieve user Complete
    postUri = '/users/' + kwargs['domainUid'] + '/' + userbmUid + '/complete'
    r = requests.get( base + postUri,verify=Verify, headers=Headers).json()
    values = r['value']
            
    ## Step 2 update user Complete   
            
    values.update(newValues)
            
    # Step 3 post user Complete
    postUri = '/users/' + kwargs['domainUid'] + '/' + userbmUid
    r = requests.post(base + postUri,verify=Verify, json=values, headers=Headers)
        
    if r.status_code == 200:
        log = "Move " + kwargs['login'] + " to new OU : " +  kwargs['ou'] 
    else:
        log = "WARNING : Code " + str(r.status_code) + " on moving " + kwargs['login'] + " to new OU : " +  kwargs['ou'] 
        
      
        
    trace.append(log)
        
    Line = ", ".join(trace)
        
    if kwargs['Verbose'] :
        print(Line)
        
    Line += "\n"    
    kwargs['outputFileDescriptor'].write(Line)
        
        
"""
(void) main:
    the main function, in which all stuff is done/called

"""
def main(argv, **kwargs):
    inputfile = None
    outputfile = None

    # Define here "blabla" when options are not defined or help called 
    usage='''
        This script can migrate an account from an Ou to a newer one.
        
        ALL NEW OU MUST BE ALREADY CREATED ON BLUEMIND SERVER, BEFORE USE.
        DON'T FORGET IN BM, THERE IS NO STARTING "/" (SLASH)
    
        Use this script as :

            python moveFromOuToOtherOne.py -i|--ifile <inputfile> -o|--ofile <outputfile> [-h] [-v]
    
        Where:
            <inputfile> : is the name of the csv file containing data (user;New ouName), e.g. : marie.curie;FR/NUCLEAR
            <outputfile> : is the name of the file where to store results
    '''

    try:
        opts, args = getopt.getopt(argv,"hi:o:v",["ifile=","ofile="])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    Verbose = False 

    for opt, arg in opts:
        if opt == '-h':
            print(usage) 
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt == '-v':
            Verbose = True

    if inputfile is None:
        print(usage)
        sys.exit()
        
    if  outputfile is None:
        print(usage) 
        sys.exit()

    # Set all common params to all servers 
    params = kwargs 
    params['Verbose'] = Verbose
    params['admin']    = kwargs['adminAccount']['admin']
    params['adminPwd'] = kwargs['adminAccount']['adminPwd']
    params['authKey'] = getToken(**dict(params))
    
    params['domainUid'] = kwargs['adminAccount']['domain']

    #Open result file
    f = openWriteFile(outputfile)
    params['outputFileDescriptor'] = f
    
    listOfOu = getAllOU(**kwargs)
    
    # Read CSV file
    for row in readCsv(inputfile):  
        # Define all params for this account
        params['login'] = row[0]
        params['ouUid'] = listOfOu[row[1]]
        params['ou']    = row[1]
        
        # UPdate the entry on server
        updateUser(**dict(params))

 
    f.close

################################ The Main Program start here ################################ 
 


if __name__ == "__main__":    
    
    params = { 'server' : server ,
               'adminAccount' : adminAccount,
               'VerifyPeer':VerifyPeer,
               'origin':origin,
               'Headers': Headers,
               }
    
    main(sys.argv[1:], **dict(params))
