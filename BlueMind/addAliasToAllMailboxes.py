#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
*------------------------------------------------------------------------
* addAliasToAllMailboxes
*------------------------------------------------------------------------
* @license : GNU Affero General Public License Version3 <http://www.gnu.org/licenses>
* @author  : Pascal SALAUN <pascal.salaun@bm-monitor.org>
* @Website : http://bm-monitor.org
*------------------------------------------------------------------------
"""


################################ Define all options here ################################ 

## Domain Alias 
domainAlias="bm90-deb9.intra"


## BLUEMIND

# Server (IP or FQDN) where you want to create user accounts, primary first.
server              = 'bm4-deb9.intra'

# Common domain & admin account to all servers
adminAccount          = {
                            'domain': 'bm4-deb9.intra' , 
                            'admin': 'admin@bm4-deb9.intra' , 
                            'adminPwd' : 'admin'
                        }


# Disable certificate control
VerifyPeer = False 

# Define a specific source name (cf BM core.log)
origin = "bm-monitor-addAliasToAllMailboxes" 

# Define the common headers values to all requests. The session token 'X-BM-ApiKey' will be added after. 
Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache'} 


################################ Define all Python lib import ################################ 

# Native Python Lib/Class
import getopt 
import json
import os.path 
import sys
import uuid

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)



################################ Define all functions here ################################ 


"""
(obj) openWriteFile:
    open a file object in "write" mode, and return it
    Args:
        outputfile : the path and name to res file

"""
def openWriteFile(outputfile):
    f = open(outputfile,'w')
    return f



"""
(str) getToken :
    call BM server and ask a session token and return it
    kwargs:  
        server     : the BM server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        admin      : the same admin account available on each server
        adminPwd   : its password
"""
def getToken(**kwargs): 
    Verify   = kwargs['VerifyPeer']
    response = {}
    response = {'authKey': None}
    
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': "application/json"}
    postUri = '/auth/login?login=' + kwargs['admin'] + '&origin=' + kwargs['origin']

    Data = None
    Data = json.dumps(kwargs['adminPwd'])

    server =  kwargs['server']
    base   = 'https://' + server + '/api'
    
    response[server] = {}

    r = requests.post( base + postUri,verify=Verify, headers=Headers, data = Data ).json()

    if r['authKey']:
        response = r['authKey']
    
    return response 



"""
(list) getAllUids :
    Retrieve all uids from server except "bmhiddensysadmin"
    Return them in a list 
    kwargs :
        server      : the BM server defined in global options
        authKey     : the session token for the server
        VerifyPeer  : True or False, disable or not server cert control
        domainUid   : the domain uid (the FQDN)
    
"""
def getAllUids(**kwargs):
    base="https://" + kwargs['server'] + "/api"
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']

    postUri = '/users/' + kwargs['domainUid'] + '/_alluids'

    r = requests.get( base + postUri,verify=Verify, headers=Headers).json()
    
    uids = []
    for entry in r:
        if entry != "bmhiddensysadmin":
            uids.append(entry)
            
    return uids
          



"""
(dict) getComplete : 
    Retrieve the complete dict data for this uid (aka account)
    kwargs :
        server              : the BM server defined in global options 
        domainUid           : the domain uid (the FQDN)
        authKey             : the session token for the server
        VerifyPeer          : True or False, disable or not server cert control
        uuid                : the account uuid
"""
def getComplete(**kwargs): 
    value= {}
    base="https://" + kwargs['server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = VerifyPeer

    results = {} 

    getUri = '/users/' + kwargs['domainUid'] + "/" +  kwargs['uuid'] + "/complete"
    
    r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    
    if r['value']['login'] is not None:
        value = r['value']
    
    return value
    


    

"""
(void) postComplete : 
    post to BM server the updated user infos, and return requests response object 
    kwargs :
        domainAlias  : the domain alias
        authKey      : the session token for the server
        Headers      : Headers defined in global options
        server       : the BM server defined in global options
        origin       : origin defined in global options
        VerifyPeer   : True or False, disable or not server cert control
        domainUid    : the domain uid (the FQDN)
        completeUser : the BM account complete request result             
        uuid         : the account uuid       
"""
def postComplete(**kwargs):    
    login = kwargs['completeUser']['login']
    uuid  = kwargs['uuid']
    domainAlias = kwargs['domainAlias']
    
    alias = login + "@" + domainAlias
    
    email_alias =  { 'address': alias, 'allAliases': 'false', 'isDefault': 'false' }
    kwargs['completeUser']['emails'].append(email_alias)

    contactInfos_email_alias =  {'parameters': [ {'label': 'DEFAULT', 'value': 'false'}, { 'label': 'SYSTEM', 'value': 'false'}, {'label': 'TYPE', 'value': 'work'}],'value': alias }
    kwargs['completeUser']['contactInfos']['communications']['emails'].append(contactInfos_email_alias)

    base="https://" + kwargs['server'] + "/api"
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']
   
    postUri = '/users/' + kwargs['domainUid'] + '/' + uuid

    r = requests.post(base + postUri,verify=Verify, json=kwargs['completeUser'], headers=Headers)    
    writeLog(rr=r, **kwargs)
 


"""
(void) writeLog : 
    Write logs in outputfile
    Print log on screen if Verbose set to True
    kwargs :
        completeUser          : dict returned by BM API "/user/complete", containing all infos about account
        uuid                  : account UUID
        rr                    : BM request result
        outputFileDescriptor  : the outputfile handler
        Verbose               : print log or not on screen
"""
def writeLog(**kwargs):
    login = kwargs['completeUser']['login']
    uuid  = kwargs['uuid']
    rr    = kwargs['rr']
    
    status = "" 
    line   = "" 
    
    if rr.status_code == 200:
        if len(rr.text) == 0:
            status = "200, OK"
        else:
            status = "200, "+ rr.text
    else:
        status = str(rr.status_code) + ", " + rr.text
        
    
    Line = login + ", " + uuid + ", " + kwargs['domainAlias'] +  ", " + status
    
    if kwargs['Verbose']:
        print(Line)
    
    Line += "\n"
    kwargs['outputFileDescriptor'].write(Line)
    
    

"""
(void) main:
    the main function, in which all stuff is done/called

"""
def main(argv, **kwargs):
    outputfile = None
    Verbose = False 
     
    # Define here "blabla" when options are not defined or help called 
    usage='''
        Use this script as :

            python3 addAliasToAllMailboxes.py -o|--ofile <outputfile> [-d|--domainAlias <domainAlias>] [-h] [-v]
    
        Where:
             <outputfile> : is the name of the file where to store results
             <domainAlias> : is the new domain alias you want accounts to be added to
    '''

    try:
        opts, args = getopt.getopt(argv,"ho:d:v",["ofile=", "domainAlias="])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)  

    for opt, arg in opts:
        if opt == '-h':
            print(usage) 
            sys.exit()
        elif opt in ("-o", "--ofile"):
            outputfile = arg
        elif opt in ("-d", "--domainAlias"):
            kwargs['domainAlias'] = arg            
        elif opt == '-v':
            Verbose = True

    if  outputfile is None:
        print(usage) 
        sys.exit()

    # Set all common params to all servers 
    params             = kwargs 
    params['Verbose']  = Verbose
    params['admin']    = kwargs['adminAccount']['admin']
    params['adminPwd'] = kwargs['adminAccount']['adminPwd']
    params['authKey']  = getToken(**dict(params))
    
    params['domainUid'] = kwargs['adminAccount']['domain']
    
    #Open result file
    f = openWriteFile(outputfile)
    params['outputFileDescriptor'] = f

    allUuids = getAllUids(**params)
    
    for uuid in allUuids:         
        completeUser = getComplete(uuid = uuid, **params)
        postComplete(uuid=uuid, completeUser=completeUser, **dict(params))
        
    f.close



################################ The Main Program start here ################################ 
 


if __name__ == "__main__":    
    
    params = { 'server' : server ,
               'adminAccount' : adminAccount,
               'VerifyPeer':VerifyPeer,
               'origin':origin,
               'Headers': Headers,
               'domainAlias': domainAlias, 
               }
    
    main(sys.argv[1:], **dict(params))
