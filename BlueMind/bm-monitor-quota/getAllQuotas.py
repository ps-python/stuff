#!/usr/bin/python3
# -*- coding: utf-8; py-indent-offset: 4 -*-


#  +-- bm-monitor -------------------------------------------------------------+
#  |                                                                           |
#  |  _                                                                        |
#  | | |                                             _                         | 
#  | | |___  _ __ ___       _ __ ___   ___  _ __  _ | |    ___  _ _            |
#  | |  _  \| '_ ` _ \  __ | '_ ` _ \ / _ \| '_ \| ||  _| / _ \| `_|           |
#  | | |_| || | | | | ||__|| | | | | | |_| | | | | || |__ ||_||| |             |
#  | |_____/|_| |_| |_|    |_| |_| |_|\___/|_| |_|_||____|\___/|_|             |
#  |                                                                           |
#  |                                                                           |
#  +---------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * bm-monitor is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  bm-monitor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


#  +-- Howto ------------------------------------------------------------------+
#
#  Need python lib. You can install them as after:
#
#       pip install requests
#           or    
#       apt install python3-requests
#
#  +---------------------------------------------------------------------------+


################################ Define all Python lib import ################################ 

# Native Python Lib/Class
from datetime import datetime, date
import json
import math
import os
import time

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


import smtplib
from pathlib import Path
from email import encoders
from email.mime.application import MIMEApplication
from email.mime.multipart  import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders as Encoders
from email.mime.text import MIMEText

################################ Define all options here ################################ 

## Global Variables ## 
# Disable certificate control
VerifyPeer = False 

# Define a specific source name (cf BM core.log)
origin = "bm-monitor-quota" 

# Define the common headers values to all requests. The session token 'X-BM-ApiKey' will be added after. 
Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache'} 

# Set today date 
today       = date.today()


## BLUEMIND Specs ## 
# Server (IP or FQDN) where you want to get quotas
Server               = '1.2.3.4'

# Global admin specs
adminGlobal = { 'account': 'admin0@global.virt', 
                'password' : 'itsPassword'
} 


## Storage method (csv, elk) ## 
storeMethod='csv' # Could be "elk", in this case update "params" dict (cf L528)
todayDateCsvFileName = today.strftime("%Y%m%d")

# -- CSV case --
# Set which domain exclude from alert 
notThisDomain = ['global.virt']

# Mail Specs

# mail sender account specs
senderAccount = { 'account': 'albert.einstein@atomium.be', 
                  'password' : 'itsPassword'
} 

# List of Cc & Bcc destinators
ccAccounts = [] #['support@other.domain.tld']
bccAccounts = [] #['support@other.domain.tld']



# mail content 
#SUBJECT_BOQ = '[BLUEMIND] : le quota du compte __mailbox__ a atteint les ' + str(quotaThresholdAverage) + '%'
SUBJECT = '[BLUEMIND] : ETAT DES QUOTA AU ' + today.strftime("%d/%m/%Y")


BODYTEXT ='Bonjour,\n'
BODYTEXT +='\n'
BODYTEXT +='\n'
BODYTEXT +='voici un compte-rendu des quota appliqués et utilisés des comptes de votre domaine.\n'
BODYTEXT +='\n'
BODYTEXT +='Faites attention aux comptes ayant un quota illimité dont les valeurs ne peuvent être remontées.\n'
BODYTEXT +='\n'
BODYTEXT +='\n'
BODYTEXT +='\n'
BODYTEXT +='L\'Equipe Messagerie'



# -- Elastic Storage Case -- 
elasticURI  = "http://192.168.122.131:9200" # Without end slash 
indexName   = "bmmonitorquotas"
todayNumELK = today.strftime("%Y.%m.%d")










################################ Define all functions here ################################ 

"""
(dict) getToken :
    call BM servers and ask a session token and return all in a dict {'serverA': 'tokenA', 'serverB': 'tokenB',...}
    Return a dict of token (value) by server (key)
    kwargs:  
        Server     : Server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        account      : the same admin account available on each server
        password   : its password
"""
def getToken(**kwargs): 
    Verify   = kwargs['VerifyPeer']
    response = {'authKey': None}
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': "application/json"}
    postUri = '/auth/login?login=' + kwargs['account'] + '&origin=' + kwargs['origin']

    Data = None
    Data = json.dumps(kwargs['password'])
    base = 'https://' + Server + '/api'

    r = requests.post( base + postUri,verify=Verify, headers=Headers, data = Data ).json()

    if r['authKey']:
        response['authKey'] = r['authKey']

    return response 



"""
(list) retrieveAllDomains :
    call BM servers and ask all domains
    Return the list of domains 
    kwargs:  
        Server     : Server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        authKey    : the session authkey
"""
def retrieveAllDomains(**kwargs):
    base="https://" + kwargs['Server'] + "/api"
    origin = kwargs['origin']
    notThisDomain = kwargs['notThisDomain']

    domains = []
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']
    
    getUri = '/domains'
    r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    
    if len(r) > 0:
        for domain in r:
            if domain['value']['name'] not in notThisDomain:
                domains.append(domain['uid'])
    
    return domains

    

"""
(list) getAllMailboxAndUID :
    call BM servers and ask all mailboxes for a domain
    Return the list of of dicts {"name":<mailbox name>, "uid": <mailbox uid>}
    kwargs:  
        Server     : Server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        authKey    : the session authkey
        domain     : the domain uid
"""
def getAllMailboxAndUID(**kwargs):
    """
    
    """
    results = []
    entry = {}

    base="https://" + kwargs['Server'] + "/api"
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  =  kwargs['VerifyPeer']

    postUri = '/mailboxes/' + kwargs['domain'] + '/_list'

    r = requests.get( base + postUri,verify=Verify, headers=Headers).json()
   
    try:
        for entry in r:
            if not entry['value']['hidden']:
                l = { "name" : entry['value']['name'], "uid":entry['uid'] }
                results.append(l)
    except: 
        pass
    
    return results



"""
(dict) getMBQuota :
    call BM servers and ask quota figures for a mailbox
    Return a dict {"quota":<quota in Ko>, "used": <used space in Ko>, "ratio": <used average in %>, "quotaHR": <pretty readable quota> }
    kwargs:  
        Server     : Server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        authKey    : the session authkey
        domain     : the domain uid
        uid        : the mailbox uid
"""
def getMBQuota(**kwargs):
    """
    
    """
    # From BM : quota in ko, used in ko
    # {'quota': 10240, 'used': 442}
     
    values = {'quota': 0 , 'used':0 , 'ratio': 0 } 
   
    base="https://" + kwargs['Server'] + "/api"
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']
    
    getUri = '/mailboxes/' + kwargs['domain'] + "/" +  kwargs['uid'] + "/_quota"
    
    r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    
    values['used'] = r['used']
    if r['quota'] is not None:
        values['quota']   = r['quota']
        values['quotaHR'] = str(convert_size(int(r['quota'])*1024))
        values['ratio']   = math.floor( (values['used'] / r['quota']) * 10000) / 100
        
    return values



def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])



"""
(void) requestToElk :
    Post mailbox figures to Elasticsearch index
    kwargs:  
        elasticURI  : the URI of elascticsearch isntance 
        indexName   : the elascticsearch index name, without "-" ans "*"
        todayNumELK : the attending date for index name
        json        : the dict containing mailbox figures and stuff for search 

"""
def requestToElk(**kwargs):    
    url = kwargs['elasticURI'] + "/" + kwargs['indexName'] + "-" + kwargs['todayNumELK'] + "/_doc/"

    try:
        res = requests.post(url, json=kwargs['json'])
    except Exception as error:
        pass



"""
(void) csvFileName : 
    Write line in domain csv file 
    kwargs :
        handler : this formated day 
        

"""
def feedCsv(**kwargs):
    if  kwargs['name'] not in ('_user','_admin','bmhiddensysadmin'):
        thisLine = ""
        thisLine = kwargs['name'] + ',' + str(kwargs['quota']) + ',' + str(kwargs['used']) + ',' + str(kwargs['ratio']) + '\n'
        kwargs['handler'].write(thisLine)



"""
(dict) createCsvFile : 
    Return a dict containing the open file handler and the file name 
    kwargs :
        domain               : the domain uid (the FQDN)
        todayDateCsvFileName : this formated day 
"""
def createCsvFile(**kwargs):
    csvDict= {'handler': '', 'csvFileName': ''}
    todayDateCsvFileName = kwargs['todayDateCsvFileName']
    
    csvDict['csvFileName'] = '/tmp/' + kwargs['domain'] + '_quotaOverview-' + todayDateCsvFileName + '.csv'
    csvDict['handler'] = open(csvDict['csvFileName'], "w")
    
    Header = 'account,quota,used,ratio\n'
    csvDict['handler'].write(Header)

    return csvDict






"""
(str) getAccountNameFromUID:
    Retrieve and return the main email from its UID 
    kwargs :
        Server     : the server
        authKey    : the session authkey
        VerifyPeer : True or False, disable or not server cert control
        domain     : the domain uid (the FQDN)
        uid
"""
def getAccountMailboxFromUID(**kwargs):
    base="https://" + kwargs['Server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = VerifyPeer

    getUri = '/users/' + kwargs['domain'] + "/" +  kwargs['uid'] + "/complete"

    try:
    	r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    	if 'value' in r.keys():
    	    if r['value']['emails'][0]['address'] is not None:
                return r['value']['emails'][0]['address']
    except:
        pass


"""
(list) getDomainAdminGroupUid : 
    get 
    kwargs :
        Server     : the server
        domain     : the domain uid (the FQDN)
        authKey    : the session token for the server
        VerifyPeer : True or False, disable or not server cert control       
"""
def getDomainAdminEmails(**kwargs):
    
    admingroupPrettyName = 'admin'
    emails = []
    
    Verify     = kwargs['VerifyPeer']
    base = "https://" + kwargs['Server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    getUri = '/groups/' + kwargs['domain'] + '/byName/' + admingroupPrettyName

    r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    if 'uid' in r.keys():
        getUri = '/groups/' + kwargs['domain'] + '/' +  r['uid']  + '/members'
        r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
        
        for account in r:
            emails.append(getAccountMailboxFromUID(**kwargs, uid=account['uid']))
        
    return emails

"""
(void) sendMessage : 
    Try to send a message about some mailbox quota information 
    kwargs :
        Server     : the server
        domain     : the domain uid (the FQDN)
        authKey    : the session token for the server
        VerifyPeer : True or False, disable or not server cert control
        sender     : The sender mail address used to connect & authenticate to BM
        senderPwd  : Its password
        Subject    : The mail SUBJECT 
        To         : A list of destinators
        Cc         : A list of carbon copy destinators
        body       : The body mail content
        csvFileName: The name of the CSV attached to mail
        
"""
def sendMessage(**kwargs):
    to = []
    msg = MIMEMultipart()
    msg['Subject'] = kwargs['Subject']
    msg['From']    = kwargs['sender']
    pathToCsvFile  = kwargs['csvFileName']
    csvFileName    = Path(pathToCsvFile).name
    
    if len(kwargs['To']) > 0:
        msg['To']      = ', '.join(kwargs['To'])
        to.extend(kwargs['To'])
    else:
        msg['To'] = kwargs['sender']
        to.append(kwargs['sender'])
    

    if len(kwargs['ccAccounts']) > 0:
        msg['Cc'] = ', '.join(kwargs['ccAccounts'])
        to.extend(kwargs['Cc'])

    if len(kwargs['bccAccounts']) > 0:
        msg['Bcc'] = ', '.join(kwargs['bccAccounts'])
        to.extend(kwargs['bccAccounts'])

    msg.attach(MIMEText(kwargs['body'], 'plain'))

    with open(pathToCsvFile) as fp:
        record = MIMEBase('application', 'octet-stream')
        record.set_payload(fp.read())
        encoders.encode_base64(record)
        record.add_header('Content-Disposition', 'attachment', filename=csvFileName)
    msg.attach(record)
    
    text = msg.as_string()

    try:
        server = smtplib.SMTP(kwargs['Server'])
        server.starttls()
        server.login(kwargs['sender'], kwargs['senderPwd'])
        server.sendmail(kwargs['sender'], to, text)
        server.quit()
    except:
        pass






"""
(void) main:
    the main function, in which all stuff is done/called
"""
def main(**kwargs):
    # Set all common params to all servers 
    params = kwargs 
    params['account']  = kwargs['adminGlobal']['account']
    params['password'] = kwargs['adminGlobal']['password']
    params.update(getToken(**dict(params)))
    
    domains = retrieveAllDomains(**params)

    for domain in domains:
        mbx = getAllMailboxAndUID(**params, domain=domain)
        if 'csv' in storeMethod:
            d_csv = createCsvFile(**params, domain=domain)
            
            #mailParams = getDomainMailParams(**params, domain=domain)
        if len(mbx) > 0:
            for MB in mbx:
                msg = {}
                msg['@timestamp']  = int(time.time())*1000
                msg['source']      = 'bmMonitor'
                msg['domain']      = domain
                msg.update(MB)
                uid = MB['uid']
                msg.update(getMBQuota(**params, domain=domain, uid=uid))


                if 'csv' in storeMethod:
                    feedCsv(**msg, handler=d_csv['handler'], todayDateCsvFileName=params['todayDateCsvFileName'])
                
                if 'elk' in storeMethod:
                    requestToElk(elasticURI=kwargs['elasticURI'], indexName=kwargs['indexName'], todayNumELK=kwargs['todayNumELK'], json=msg)

        
        if 'csv' in storeMethod:
            To = getDomainAdminEmails(**params, domain=domain)
            d_csv['handler'].close()
            sendMessage(**params, domain=domain, To=To, csvFileName=d_csv['csvFileName'])
            os.remove(d_csv['csvFileName']) 



################################ The Main Program start here ################################ 
 

if __name__ == "__main__":    
    params = { 'Server' : Server ,
               'adminGlobal' : adminGlobal,
               'VerifyPeer':VerifyPeer,
               'origin':origin,
               'Headers': Headers,
               'notThisDomain': notThisDomain,
               'storeMethod': storeMethod, 
               #'elasticURI':elasticURI, # ELK
               #'indexName':indexName, # ELK
               #'todayNumELK':todayNumELK, # ELK
               'todayDateCsvFileName': todayDateCsvFileName, # CSV 
               'Subject': SUBJECT, # CSV
               'body': BODYTEXT, # CSV
               'sender': senderAccount['account'], # CSV
               'senderPwd': senderAccount['password'], # CSV
               'bccAccounts': bccAccounts, # CSV
               'ccAccounts': ccAccounts, # CSV
               }
    main(**dict(params))

