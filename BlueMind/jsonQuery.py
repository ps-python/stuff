#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
 * ------------------------------------------------------------------------
 * bmSearch
 * ------------------------------------------------------------------------
 * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>
 * @author  : Pascal SALAUN 
 * @Website : http://www.bm-stats.org <http://www.bm-stats.org>
 * ------------------------------------------------------------------------
"""

from collections import defaultdict

js = defaultdict(dict)

js['ouSearch'] = {
  'order': {
    'by': 'kind',
    'dir': 'asc'
  },
  'kindsFilter': ['ORG_UNIT']
}

js['userGroupUidSearch'] = {
  'name': 'user'
}


js['userGroupAddUid'] = [
  {
    "type": "user",
    "uid": "string"
  }
]


js['addForwardMailbox'] = {
  'forwarding': {
    'enabled': True,
    'localCopy': False,
    'emails': [
      'albert.einstein@atomium.be'
    ]
  }
}

js['newUserTpl'] = {
    'orgUnitUid': None,
    'emails': [
      {
        'address': 'address',
        'allAliases': False,
        'isDefault': True
      }
    ],
    'hidden': False,
    'archived': False,
    'system': False,
    'dataLocation': 'bm-master',
    'login': 'login',
    'password': 'password',
    'contactInfos': {
      'kind': 'individual',
      'source': 'source' ,
      'identification': {
        'formatedName': {
          'parameters': [],
          'value': 'login'
        },
        'name': {
          'parameters': [],
          'value': None,
          'familyNames': 'login',
          'givenNames': None,
          'additionalNames': None,
          'prefixes': None,
          'suffixes': None
        },
        'nickname': {
          'parameters': [],
          'value': None
        },
        'photo': False,
        'birthday': None,
        'anniversary': None,
        'gender': {
          'parameters': [],
          'value': None,
          'text': None
        }
      },
      'deliveryAddressing': [
        {
          'address': {
            'parameters': [
              {
                'label': 'TYPE',
                'value': 'work'
              }
            ],
            'value': None,
            'postOfficeBox': 'postOfficeBox',
            'extentedAddress': None,
            'streetAddress': 'streetAddress',
            'locality': 'locality',
            'region': None,
            'postalCode': 'postalCode',
            'countryName': None
          }
        }
      ],
      'communications': {
        'tels': [
          {
            'parameters': [
              {
                'label': 'TYPE',
                'value': 'voice'
              },
              {
                'label': 'TYPE',
                'value': 'work'
              }
            ],
            'value': 'phoneWork',
            'ext': None
          }
        ],
        'emails': [
          {
            'parameters': [
              {
                'label': 'DEFAULT',
                'value': True
              },
              {
                'label': 'SYSTEM',
                'value': True
              },
              {
                'label': 'TYPE',
                'value': 'work'
              }
            ],
            'value': 'address'
          }
        ],
        'impps': [],
        'langs': []
      },
      'organizational': {
        'title': None,
        'role': None,
        'org': {
          'company': None,
          'division': None,
          'department': None
        },
        'member': []
      },
      'explanatory': {
        'urls': [],
        'categories': [],
        'note': None
      },
      'related': {
        'spouse': None,
        'manager': None,
        'assistant': None
      }
    },
    'routing': 'internal',
    'accountType': 'FULL',
    'quota': 'quota',
    'properties': {}
}
      
      
js['externalAccount'] = {
  "login": "string",
  "credentials": "string",
  "additionalSettings": {}
}
