#!/usr/bin/env python
# -*- coding: utf-8 -*-


#  +-- bm-monitor ----------------------------------------------------------------+
#  |                                                                              |
#  |  _                                                                           |
#  | | |                                                _                         | 
#  | | |___  _ __ ___          _ __ ___   ___  _ __  _ | |    ___  _ _            |
#  | |  _  \| '_ ` _ \   __   | '_ ` _ \ / _ \| '_ \| ||  _| / _ \| `_|           |
#  | | |_| || | | | | | |__|  | | | | | | |_| | | | | || |__ ||_||| |             |
#  | |_____/|_| |_| |_|       |_| |_| |_|\___/|_| |_|_||____|\___/|_|             |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * bm-monitor is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  bm-monitor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*


################################ Define all Python lib import ################################ 

# Native Python Lib/Class
import json
import math
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

import smtplib
from email.mime.multipart  import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders as Encoders
from email.mime.text import MIMEText


################################ Define all options here ################################ 

## BLUEMIND

# Server (IP or FQDN) where you want to get quotas
Server               = 'some.server.tld'

# Global admin specs
adminGlobal = { 'account': 'admin0@global.virt', 
                'password' : 'admin'
} 

# Disable certificate control
VerifyPeer = False 

# Define a specific source name (cf BM core.log)
origin = "bm-monitor-alertQuota" 

# Define the common headers values to all requests. The session token 'X-BM-ApiKey' will be added after. 
Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache'} 

# Set when alert
quotaThresholdAverage=90

# Set which domain exclude from alert 
notThisDomain = ['global.virt']


## Mail Specs

# mail sender account specs
senderAccount = { 'account': 'admin@some.server.tld', 
                  'password' : 'admin'
} 

# List of Bcc destinators
bccAccounts = ['support@other.domain.tld']

# mail content 
SUBJECT_BOQ = '[BLUEMIND] : le quota du compte __mailbox__ a atteint les ' + str(quotaThresholdAverage) + '%'
SUBJECT_OQ  = '[BLUEMIND] : le compte __mailbox__ est over-quota'


BODYTEXT ='Bonjour,\n'
BODYTEXT +='\n'
BODYTEXT +='\n'
BODYTEXT +='merci de prendre toutes les mesures nécessaires quant à la bonne gestion de ce compte '
BODYTEXT +='\n'
BODYTEXT +='\n'
BODYTEXT +='\n'
BODYTEXT +='L\'Equipe Messagerie'


################################ Define all functions here ################################ 

"""
(dict) getToken :
    call BM servers and ask a session token and return all in a dict {'serverA': 'tokenA', 'serverB': 'tokenB',...}
    Return a dict of token (value) by server (key)
    kwargs:  
        Server     : Server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        account      : the same admin account available on each server
        password   : its password
"""
def getToken(**kwargs): 
    Verify   = kwargs['VerifyPeer']
    response = {'authKey': None}
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': "application/json"}
    postUri = '/auth/login?login=' + kwargs['account'] + '&origin=' + kwargs['origin']

    Data = None
    Data = json.dumps(kwargs['password'])
    base = 'https://' + Server + '/api'

    r = requests.post( base + postUri,verify=Verify, headers=Headers, data = Data ).json()

    if r['authKey']:
        response['authKey'] = r['authKey']

    return response 


"""
(list) retrieveAllDomains :
    call BM servers and ask all domains
    Return the list of domains 
    kwargs:  
        Server     : Server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        authKey    : the session authkey
"""
def retrieveAllDomains(**kwargs):
    base="https://" + kwargs['Server'] + "/api"
    origin = kwargs['origin']
    notThisDomain = kwargs['notThisDomain']
    
    domains = []
    
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']
    
    getUri = '/domains'
    r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    
    if len(r) > 0:
        for domain in r:
            if domain not in notThisDomain:
                domains.append(domain['uid'])
    
    return domains

    

"""
(list) getAllMailboxAndUID :
    call BM servers and ask all mailboxes for a domain
    Return a dict containing 
        - 'mbx' : a list of dicts {"name":<mailbox name>, "uid": <mailbox uid>, "ou": <orgaUnit uid>}
    kwargs:  
        Server     : Server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        authKey    : the session authkey
        domain     : the domain uid
"""
def getAllMailboxAndUID(**kwargs):
    """
    
    """
    null='null'
    results = []
    entry = {}
    uidToMailbox = {}
    base="https://" + kwargs['Server'] + "/api"
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  =  kwargs['VerifyPeer']

    postUri = '/mailboxes/' + kwargs['domain'] + '/_list'

    r = requests.get( base + postUri,verify=Verify, headers=Headers).json()
   
    try:
        for entry in r:
            if not entry['value']['hidden']:
                if  entry['value']['quota'] is not None: 
                    l = { "name" : entry['value']['name'], "uid":entry['uid'], "ou": getMailboxCompleteLeafFromUID(uid=entry['uid'], **kwargs) }
                    results.append(l)
    except: 
        pass
    
    RES = {}
    RES['mbx'] = results

    return RES



"""
(str) getMailboxOrgaUnit :
    Return the orgaUnit uid of this mailbox 
    kwargs:  
        Server     : Server defined in global options
        uid        : the mailbox uid
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        authKey    : the session authkey
        domain     : the domain uid
"""
def getMailboxCompleteLeafFromUID(**kwargs):
    """
    
    """
    value = {}
    
    base="https://" + kwargs['Server'] + "/api"
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  =  kwargs['VerifyPeer']

    getUri = '/users/' + kwargs['domain'] + '/' + kwargs['uid'] +'/complete'

    r = requests.get( base + getUri,verify=Verify, headers=Headers).json()

    if r['value']['orgUnitUid'] is not None :
        value['ou']    = r['value']['orgUnitUid']
    else:
        value['ou'] = "root"
            
    return value['ou']
    
    

"""
(dict) getMBQuota :
    call BM servers and ask quota figures for a mailbox
    Return a dict {"quota":<quota in Ko>, "used": <used space in Ko>, "ratio": <used average in %>, "quotaHR": <pretty readable quota> }
    kwargs:  
        Server     : Server defined in global options
        origin     : a name for core.log (defined in options#origin
        VerifyPeer : True or False, disable or not server cert control
        authKey    : the session authkey
        domain     : the domain uid
        uid        : the mailbox uid
"""
def getMBQuota(**kwargs):
    """
    
    """
    # From BM : quota in ko, used in ko
    # {'quota': 10240, 'used': 442}
     
    values = {'quota': 0 , 'used':0 , 'ratio': 0 } 
   
    base="https://" + kwargs['Server'] + "/api"
    origin = kwargs['origin']

    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = kwargs['VerifyPeer']
    
    getUri = '/mailboxes/' + kwargs['domain'] + "/" +  kwargs['uid'] + "/_quota"
    
    r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    
    values['used'] = r['used']
    if r['quota'] is not None:
        values['quota']   = r['quota']
        values['quotaHR'] = str(convert_size(int(r['quota'])*1024))
        values['ratio']   = math.floor( (values['used'] / r['quota']) * 10000) / 100

    return values



"""
(str) convert_size : 
    return a pretty size from bytes 
    kwargs :
        size_bytes : a number of bytes
"""
def convert_size(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])



"""
(dict) getAllOUAdmins : 
    retrieve all unitOrganization from Domain, and return dict {"ou": "ouUUID", }
    kwargs :
        Server     : the server
        domain     : the domain uid (the FQDN)
        authKey    : the session token for the server
        VerifyPeer : True or False, disable or not server cert control
"""
def getAllOUAdmins(**kwargs):
    Verify     = kwargs['VerifyPeer']

    base = "https://" + kwargs['Server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}

    postUri = '/directory/' + kwargs['domain'] + '/_search'
    jsquery = {
                'order': {
                    'by': 'kind',
                    'dir': 'asc'
                },
                'kindsFilter': ['ORG_UNIT']
            }

    r = requests.post( base + postUri,verify=Verify, data=json.dumps(jsquery),headers=Headers).json()
    
    ouAdminList = {'root':kwargs['sender']}
    if 'values' in r.keys():
        for entry in r['values']:
            uid = entry['uid']
            adminList = []
            adminList = getOuAdministratorUID(uid=uid, **kwargs)

            if len(adminList) ==  0 : 
                if entry['value']['orgUnitPath']:
                    sub = entry['value']['orgUnitPath']
                    if 'parent' in sub:
                        while sub['parent']:
                            tuid = sub['parent']['uid']
                            adminList = getOuAdministratorUID(uid=tuid, **kwargs)
                            if len(adminList) == 0 :
                                if  sub['parent'] is not None :
                                    sub = sub['parent']
                            else:
                                break
            #ouAdminList[uid] = [ getAccountNameFromUID(uid=uid, **kwargs) for uid in adminList ]
            ouAdminList[uid] = []
            for thisUid in adminList:
                accountName = getAccountNameFromUID(uid=thisUid, **kwargs)
                if accountName is not None :
                     ouAdminList[uid].append(accountName)

    return ouAdminList



"""
(str) getAccountNameFromUID:
    Retrieve and return the login from its UID 
    kwargs :
        Server     : the server
        authKey    : the session authkey
        VerifyPeer : True or False, disable or not server cert control
        domain     : the domain uid (the FQDN)
        uid
"""
def getAccountNameFromUID(**kwargs):
    value= None
    base="https://" + kwargs['Server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}
    Verify  = VerifyPeer

    getUri = '/users/' + kwargs['domain'] + "/" +  kwargs['uid'] + "/complete"

    try:
    	r = requests.get( base + getUri,verify=Verify, headers=Headers).json()
    	if 'value' in r.keys():
    	    if r['value']['login'] is not None:
                value = r['value']['login'] + '@' +  kwargs['domain']
    except:
        pass

    return value




"""
(list) getOuAdministratorUID : 
    return a list of all unitOrganization admins uids
    kwargs :
        Server     : the server
        domain     : the domain uid (the FQDN)
        uid        : the orgaUnit uid
        authKey    : the session token for the server
        VerifyPeer : True or False, disable or not server cert control
"""
def getOuAdministratorUID(**kwargs):
    Verify     = kwargs['VerifyPeer']
    ouAdminUids = []

    base = "https://" + kwargs['Server'] + "/api"
    Headers = {'Content-Type': "application/json; charset=utf-8", 'Accept': 'application/json', 'Cache-Control': 'no-cache', 'X-BM-ApiKey':kwargs['authKey']}

    getUri = '/directory/_ou/' + kwargs['domain'] + '/' + kwargs['uid'] + '/_administrators'

    ouAdminUids = requests.get( base + getUri,verify=Verify,headers=Headers).json()

    return ouAdminUids
     


"""
(void) sendMessage : 
    Try to send a message about some mailbox quota information 
    kwargs :
        Server     : the server
        domain     : the domain uid (the FQDN)
        authKey    : the session token for the server
        VerifyPeer : True or False, disable or not server cert control
        sender     : The sender mail address used to connect & authenticate to BM
        senderPwd  : Its password
        Subject    : The mail SUBJECT 
        To         : A list of destinators
        Cc         : A list of carbon copy destinators
        body       : The body mail content
        
"""
def sendMessage(**kwargs):
    to = []
    msg = MIMEMultipart()
    msg['Subject'] = kwargs['Subject']
    msg['From']    = kwargs['sender']
    
    if len(kwargs['To']) > 0:
        msg['To']      = ', '.join(kwargs['To'])
        to.extend(kwargs['To'])
    else:
        msg['To'] = kwargs['sender']
        to.append(kwargs['sender'])
    

    if len(kwargs['Cc']) > 0:
        msg['Cc'] = ', '.join(kwargs['Cc'])
        to.extend(kwargs['Cc'])

    if len(kwargs['bccAccounts']) > 0:
        msg['Bcc'] = ', '.join(kwargs['bccAccounts'])
        to.extend(kwargs['bccAccounts'])

    msg.attach(MIMEText(kwargs['body'], 'plain'))
    text = msg.as_string()

    try:
        server = smtplib.SMTP(kwargs['Server'])
        server.starttls()
        server.login(kwargs['sender'], kwargs['senderPwd'])
        server.sendmail(kwargs['sender'], to, text)
        server.quit()
    except:
        pass


"""
(void) main:
    the main function, in which all stuff is done/called
    kwargs:
        Server                : the server
        adminGlobal           : dict containing a global admin account elements 
        VerifyPeer            : True or False, disable or not server cert control
        origin                : Some information about script to locate in bm core logs
        Headers               : HTTP Headers
        quotaThresholdAverage : (int) limit when starts alerting user about their quota
        body                  : the mail body text
        sender                : the mail address we send message with
        senderPwd             : its password
        notThisDomain         : The list of domain not to control (e.g global.virt)
        
"""
def main(**kwargs):
    # Set all common params to all servers 
    params = kwargs 
    params['account']  = kwargs['adminGlobal']['account']
    params['password'] = kwargs['adminGlobal']['password']
    params.update(getToken(**dict(params)))
    
    domains = retrieveAllDomains(**dict(params))
    
    for domain in domains:
        stuff = getAllMailboxAndUID(**params, domain=domain)
        adminList = getAllOUAdmins(**params, domain=domain)
        mbx = stuff['mbx']
        ADMINMBX = []
        
        if len(mbx) > 0:
            for MB in mbx:
                thisQuota = 0
                thisQuota = getMBQuota(uid=MB['uid'], domain=domain, **kwargs)['ratio']
                ADMINMBX = adminList[MB['ou']]
                
                if len(ADMINMBX) == 0:
                    ADMINMBX.append(kwargs['sender'])
            
                if (thisQuota >= quotaThresholdAverage) and (thisQuota < 100):
                    SUBJECT = SUBJECT_BOQ.replace('__mailbox__',MB['name'])
                    To = [ MB['name'] + '@' + domain ]
                    Cc= ADMINMBX
                    sendMessage(Subject=SUBJECT, To=To ,Cc=Cc ,**kwargs)
                    
                if thisQuota >= 100:
                    SUBJECT = SUBJECT_OQ.replace('__mailbox__',MB['name'])
                    To = ADMINMBX
                    Cc = ""  
                    sendMessage(Subject=SUBJECT, To=To ,Cc=Cc ,**kwargs)


                    



################################ The Main Program start here ################################ 
 

if __name__ == "__main__":    
    params = { 'Server' : Server ,
               'adminGlobal' : adminGlobal,
               'VerifyPeer':VerifyPeer,
               'origin':origin,
               'Headers': Headers,
               'quotaThresholdAverage': quotaThresholdAverage,
               'body': BODYTEXT,
               'sender': senderAccount['account'],
               'senderPwd': senderAccount['password'],
               'notThisDomain': notThisDomain,
               'bccAccounts': bccAccounts,
               }
    
    main(**dict(params))

