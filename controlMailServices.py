#!/usr/bin/env python3
# -*- coding: utf-8 -*-


#  +-- bm-monitor ----------------------------------------------------------------+
#  |                                                                              |
#  |  _                                                                           |
#  | | |                                                _                         | 
#  | | |___  _ __ ___          _ __ ___   ___  _ __  _ | |    ___  _ _            |
#  | |  _  \| '_ ` _ \   __   | '_ ` _ \ / _ \| '_ \| ||  _| / _ \| `_|           |
#  | | |_| || | | | | | |__|  | | | | | | |_| | | | | || |__ ||_||| |             |
#  | |_____/|_| |_| |_|       |_| |_| |_|\___/|_| |_|_||____|\___/|_|             |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * bm-monitor is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  bm-monitor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*




"""
NAGIOS Script : check mailbox service (SMTP and IMAP(s)) 

Installation:
    This script needs additional lib :
        * "mail-parser" (Cmd install : pip install mail-parser)
    
    
After :
    2 status codes only returned :
        0 : OK : all works fine
        2 : CRITICAL : Some error appeared in check proccess


Usage :
    python3 controlMailServices.py [ -m|--mailbox "alice@b.net" ] [ -w|--password trtztreter ] [ -x|--smtpServer smtp.b.net ] [ -p|--smtpPort 25 ] [ -i|--imapServer imap.b.net ] [ -P|--imapPort 993 ] [ -s|--imapSsl True|Fasle ] [ -v|--verifyPeer True|False ]
    
    
Notice :
    Use a specific account/mailbox because all message will be deleted if IMAP access is authenticated. 
    Otherwise, if you want to alter this behaviour, just add a tab at line 292 ( "self.imap.uid('STORE',muid, '+FLAGS', '(\\Deleted)')")

"""


# Python lib
from collections import defaultdict

from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate, make_msgid
from email import encoders

import getopt

import imaplib

import mailparser

import os

import smtplib
import ssl
import sys

from time import sleep

import uuid




"""
PARAMS : init the default params in this script 

"""
params = defaultdict(dict)
params = {'mailbox': "admin@bm35-deb9.intra",
          'password': "admin",
          'smtpServer': "bm35-deb9.intra",
          'smtpPort': "25",
          'imapServer': "bm35-deb9.intra",
          'imapPort': "993",
          'imapSsl': True ,
          'verifyPeer': False, #Check or not the server certificate
          'uuid': str(uuid.uuid4()).upper() #Do not change this
          }






"""
getopt
"""
options, remainder = getopt.getopt(sys.argv[1:], 'm:w:x:p:i:P:sv', 
                                   ['mailbox=', 
                                    'password=',
                                    'smtpServer=',
                                    'smtpPort=',
                                    'imapServer=',
                                    'imapPort=',
                                    'imapSsl=',
                                    'verifyPeer=',
                                    ])




for opt, arg in options:
    if opt in ('-m', '--mailbox'):
        params['mailbox'] = arg
        
    elif opt in ('-w', '--password'):
        params['password'] = arg
        
    elif opt in ('-x', '--smtpServer'):
        params['smtpServer'] = arg
        
    elif opt in ('-p', '--smtpPort'):
        params['smtpPort'] = arg
        
    elif opt in ('-i', '--imapServer'):
        params['imapServer'] = arg
        
    elif opt in ('-P', '--imapPort'):
        params['imapPort'] = arg
        
    elif opt in ('-s', '--imapSsl'):
        params['imapSsl'] = arg
        
    elif opt in ('-v', '--verifyPeer'):
        params['verifyPeer'] = arg



"""
CLASS

"""

class controlMailServices():
    def __init__(self):
        self.params = defaultdict(dict)



    def setParams(self,**kwargs):
        if 'mailbox' in kwargs.keys():
            self.params['mailbox'] = kwargs['mailbox']

        if 'password' in kwargs.keys():
            self.params['password'] = kwargs['password']

        if 'smtpServer' in kwargs.keys():
            self.params['smtpServer'] = kwargs['smtpServer']

        if 'smtpPort' in kwargs.keys():
            self.params['smtpPort'] = kwargs['smtpPort']

        if 'imapServer' in kwargs.keys():
            self.params['imapServer'] = kwargs['imapServer']

        if 'imapPort' in kwargs.keys():
            self.params['imapPort'] = kwargs['imapPort']

        if 'imapSsl' in kwargs.keys():
            self.params['imapSsl'] = kwargs['imapSsl']

        if 'verifyPeer' in kwargs.keys():
            self.params['verifyPeer'] = kwargs['verifyPeer']

        if 'uuid' in kwargs.keys():
            self.params['uuid'] = kwargs['uuid']



    def sendTest(self):
        STATUS = None

        smtpServer = self.params['smtpServer']
        smtpPort   = self.params['smtpPort']
        mailbox    = self.params['mailbox']
        password   = self.params['password']
        to         = self.params['mailbox']
        uuid       = self.params['uuid']

        msg = MIMEMultipart()

        msg['From']       = mailbox
        msg['To']         = to
        msg['Subject']    = '[TEST] : Ref. : ' + uuid
        msg['message-id'] = make_msgid()
        msg['Date']       = formatdate(localtime=True)
        body = """Bonjour,

Merci de ne pas tenir compte de ce message de test.


Le Support
"""

        msg.attach(MIMEText(body, 'plain'))

        text = msg.as_string()

        try:
            server = smtplib.SMTP(smtpServer,smtpPort)
            server.starttls()
            server.login(mailbox, password)
            server.sendmail(mailbox, to, text)
            server.quit()

            STATUS = "OK"
        
        except:
            pass

        return STATUS



    def connectImap(self):
        STATUS = None
        if self.params['imapSsl'] :
            ctx = ssl.create_default_context()
            if self.params['verifyPeer'] is False:
                ctx.check_hostname = False
                ctx.verify_mode = ssl.CERT_NONE          

            try:
                self.imap = imaplib.IMAP4_SSL(self.params['imapServer'], self.params['imapPort'], ssl_context = ctx )
                STATUS = "OK"
            except Exception as e:
                pass

        else:
            try:
                self.imap = imaplib.IMAP4(self.params['imapServer'], self.params['imapPort'])
                STATUS = "OK"
            except Exception as e:
                pass

        return STATUS



    def imapLogin(self):
        STATUS = None
        try: 
            self.imap.login(self.params['mailbox'], self.params['password'])
            STATUS ="OK"
        except Exception as e:            
            pass

        return STATUS



    def imapRetrieveMessages(self):
        STATUS = None

        self.imap.select('Inbox')

        tmp, data = self.imap.search(None, 'ALL')

        tmpUl, uidL = self.imap.uid('search', None, "ALL")
        uidList = list(uidL[0].split())

        subject = '[TEST] : Ref. : ' + self.params['uuid']
        for num in data[0].split():
            muid = uidList[int(num)-1]

            # Only IMAP after
            typ, msg_data = self.imap.fetch(num,'(RFC822)')
            mail = mailparser.parse_from_bytes(msg_data[0][1])

            if mail.subject == subject:
                STATUS = "OK"

            self.imap.uid('STORE',muid, '+FLAGS', '(\\Deleted)')
            
        self.imap.expunge()
        
        # Close for next time
        self.imap.close()
        self.imap.logout()
        return STATUS



"""
Call the test
"""

t = controlMailServices()
t.setParams(**params)

## Step 1 : try to send a message
# Send Message Status
sms = t.sendTest()
if sms is None:
    print("CRITICAL : Send Mail failed")
    exit(2)

## Step 2 : wait 10 sec., time to deliver message in mailbox
sleep(10)


## Step 3 : connect, authenticate and retrieve test message
# Imap Connection Status
ics = t.connectImap()
if ics is None:
    print("CRITICAL : IMAP Connection failed to " + params['imapServer'])
    exit(2)

# Imap Login Status
ils = t.imapLogin()
if ils is None:
    print("CRITICAL : IMAP Login failed to " + params['mailbox'])
    exit(2)

# Retrieve Message Status
rms = t.imapRetrieveMessages()
if rms is None:
    print("CRITICAL : IMAP Retrieve Mail Ref " + params['uuid'] + "failed")
    exit(2)


print("OK : IMAP Retrieved Mail Ref " + params['uuid'] + ". All works fine")
exit(0)
