#!/usr/bin/env python3
# -*- coding: utf-8 -*-


#  +-- bm-monitor ----------------------------------------------------------------+
#  |                                                                              |
#  |  _                                                                           |
#  | | |                                                _                         | 
#  | | |___  _ __ ___          _ __ ___   ___  _ __  _ | |    ___  _ _            |
#  | |  _  \| '_ ` _ \   __   | '_ ` _ \ / _ \| '_ \| ||  _| / _ \| `_|           |
#  | | |_| || | | | | | |__|  | | | | | | |_| | | | | || |__ ||_||| |             |
#  | |_____/|_| |_| |_|       |_| |_| |_|\___/|_| |_|_||____|\___/|_|             |
#  |                                                                              |
#  |                                                                              |
#  +------------------------------------------------------------------------------+
#
# * ----------------------------------------------------------------------------------------*
# * @author  : Pascal SALAUN <pascal.salaun-AT-bm-monitor.org>                              *
# * @Website : http://www.bm-monitor.org <http://www.bm-monitor.org>                        *
# * @license : GNU Affero General Public License Version 3 <http://www.gnu.org/licenses>    *
# *                                                                                         *
# * bm-monitor is free software;  you can redistribute it and/or modify it under the        *
# *  terms of the  license mentionned above.                                                *
# *  bm-monitor is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY * 
# * ----------------------------------------------------------------------------------------*



################################ Define all Python lib import ################################ 

# Native Python Lib/Class
from collections import defaultdict
import csv
from datetime import datetime, date
import getopt 
import json
import multiprocessing
import os.path 
import shlex
from subprocess import CalledProcessError, check_output
import sys
import time


################################ Define all options here ################################ 

# Path to imapsync bin
imapsync="/bin/imapsync"

# List of servers (IP or FQDN) where you want to sync mailboxes, primary first.
servers               = ['10.1.2.1','10.1.3.1']

# By Default do not delete objects on peer even if not present on source
delete = False

# Display logs while processing 
verbose = False

################################ Internal var ################################ 


# Nb of simultaneous imapsync 
try:
    max_workers = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
except KeyError:
    max_workers = multiprocessing.cpu_count() - 1


################################ Define all functions here ################################ 


"""
(list) readCsv:
    read content of <inputfile> CSV and return a list of values"
    args: 
        inputfile : the full path to the CSV file
"""
def readCsv(inputfile):
    with open(inputfile, mode='r') as infile:
        spamreader = csv.reader(infile, delimiter=';')
        for row in spamreader:
            yield row



"""
(void) imapSyncThis:
    Call imapsync PERL Script via os.system
    Args : 
        list : a list containing 
            mainServer, the source server
            mailbox, the address to sync 
            password, its password
            peerServer, the peer serveur on wich sync mailboxes
            Delete, True if want to delete objects on peerServer if do not exist on mainServer 
            Verbose, True if want to display logs on screen
            logfile, the imapsync log file
"""
def imapSyncThis(*args):     
    (mainServer,mailbox,password,peerServer,Delete, Verbose, logfile) = args[0]     
    
    if mailbox is not None and password is not None:
        
        trace = []
        
        cmd  = imapsync + " --no-modulesversion --noreleasecheck "
        cmd += "--host1 " + mainServer + " --sslargs1 SSL_verify_mode=0 --user1 " + mailbox + " --password1 '" + password + "' "
        cmd += "--host2 " + peerServer + " --sslargs2 SSL_verify_mode=0 --user2 " + mailbox + " --password2 '" + password + "' "
        cmd += "--expunge2 --delete2duplicates "
        cmd += "--automap "
        cmd += "--tmpdir /tmp/IS/temp "
        cmd += "--pidfile /tmp/IS.pid "
        cmd += "--nofoldersizes "
        cmd += "--nofoldersizesatend "
        cmd += "--exclude 'Dossiers partag&AOk-s' "
        cmd += "--exclude 'Bo&AO4-tes partag&AOk-es' "


        if Delete is True:
            cmd += "--delete2 "
        
        if logfile is True:
            cmd += "--logfile " + mailbox +  ".log"
        try:
            output = check_output(shlex.split(cmd))
            log = "Success"
        except CalledProcessError as e:
            log = "Failure, " + str(e.output)
            pass 
 
        trace.append(date.today().strftime("%Y-%m-%d"))
        trace.append(time.strftime("%H:%M:%S"))
        trace.append(mailbox)       
        trace.append(log)
        
        if Verbose:
            print(", ".join(trace) + "\n")
            


"""
(str) _usage:
    Return usage text explanation
"""    
def _usage():
    usageStr='''
        This script is based on imapsync. So, it's supposed to be installed.
        Have a look to "https://imapsync.lamiral.info/". 
        
    
        Use this script as :

            python3 imapSync.py [-h|--help] [-v|--verbose] [-m|--mainServer] [-p|--peerServer] -i|--ifile [-o|--ofile] [-d|--delete]
    
        where :
            mandatory :
                -i|--ifile, the CSV file containing mailboxes and passwords, with ";" as fields separator, e.g "albert.einstein@atomium.be;NuclearBlast666"
        
            optional :
                -h|--help, display this help
                -v|--verbose, display logs while script running
                -m|--mainServer, the source mailboxes server ohterwise from servers list (1rst element) defined above script
                -p|--peerServer, the peer mailboxes server ohterwise from servers list (2nd element) defined above script
                -o|--ofile,  set sync log file for each mailbox
                -d|--delete, if set, will delete on peer what not present on source. Normally should not be used on first and last (when migrations done) sync. 
    '''
    
    return usageStr
    


"""
(void) main:
    the main function, in which all stuff is done/called

"""
def main(argv, **kwargs):
    (inputfile, logfile, mailbox, password)  = (None, False, "", "")
    
    try:
        opts, args = getopt.getopt(argv,"hvm:p:i:o:d:",["help","verbose","mainServer=","peerServer=","ifile=","ofile=","delete"])
    except getopt.GetoptError:
        print(_usage()) 
        sys.exit(2)

    params = dict(kwargs)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print(_usage()) 
            sys.exit()
        elif opt in ("-v", "--verbose"):
            params['Verbose'] = True
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            logfile =  True
        elif opt in ("-m", "--mainServer"):
            servers[0]= arg
        elif opt in ("-p", "--peerServer"):
            servers[1]= arg
        elif opt in ("-d", "--delete"):
            params['Delete'] = True

    if inputfile is None:
        print(_usage()) 
        sys.exit()
    
    imapSyncAllInfo = []
    
    for row in readCsv(inputfile):  
        mailbox = row[0]
        password = row[1]

        imapSynTupl = [ params['servers'][0], mailbox, password, params['servers'][1],params['Delete'],params['Verbose'], logfile ]
        imapSyncAllInfo.append(imapSynTupl)
        
    if len(imapSyncAllInfo) > 0:
        p = multiprocessing.Pool(params['max_workers'])
        p.map(imapSyncThis, tuple(imapSyncAllInfo)) 



################################ The Main Program start here ################################ 
 

if __name__ == "__main__":    
    
    params = { 'servers' : servers ,
               'max_workers': max_workers,
               'Delete' : delete,
               'Verbose': verbose
               }
    
    main(sys.argv[1:], **dict(params))
